
# Push4Pool

Push4Pool is a serverless application which utilises an [AWS IoT button](https://aws.amazon.com/iotbutton/), [AWS Lambda](http://aws.amazon.com/lambda/), [AWS Parameter Store](https://docs.aws.amazon.com/systems-manager/latest/userguide/systems-manager-paramstore.html) & [AWS Step Functions](https://aws.amazon.com/step-functions/) combined with slack integration to send notifications to slack when the pool table is in use.

## What Does Push4Pool Solve?

It aims to solve the issue of needing to walk to the gamesroom to see whether the pool table is free or not. Push4Pool will send a notification(after the IoT button is pressed) to slack if the pool table is in use or if it is free.

---

## More Details

Currently the repo contains just the lambda code. In the future we will try to include cloudformation for the infrastructure to automate the deployment.

Steps on how to set up and run the code will be in the **wiki** on the left hand side.

## How it works

The [AWS IoT button](https://aws.amazon.com/iotbutton/) triggers an [AWS Step Functions](https://aws.amazon.com/step-functions/) which in turn triggers the [AWS Lambda Function](http://aws.amazon.com/lambda/). From here the Lambda will check [AWS Parameter Store](https://docs.aws.amazon.com/systems-manager/latest/userguide/systems-manager-paramstore.html), carry out some logic, and then send a **Table Free** or **Table Engaged** status to a desired slack channel. Users can then check the slack channel to see whether the table is in use or not.

The IoT button can send 3 types of click type to the Lambda Function:
- SINGLE
- DOUBLE
- LONG

A single click is configured to **ENGAGE** the table, whilst a double click will **FREE** the table. The current table status is stored in Parameter Store and the lambda will check the current status and only post to the slack channel if the status of the table changes.

More detailed instructions on setting up, testing & running the function will be in the **wiki**.

## Future work

Future work and requested features can be seen on the **Trello Board** included in the repo.
