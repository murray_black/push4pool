import os, json, boto3
from botocore.vendored import requests

channel = os.environ["channel"]
url = os.environ["webhookURL"]
ssm = boto3.client('ssm')

def lambda_handler(event, context):

    clickType = event['clickType']
    parameter = ssm.get_parameter(Name='TableStatus', WithDecryption=True)
    parameter_status = parameter['Parameter']['Value']
 
    if clickType == 'SINGLE':
        table_status = "engaged"
    else:
        table_status = "free"

    if table_status != parameter_status:
        slack_message_colour = get_message_colour(table_status)
        slack_message = create_slack_message(slack_message_colour, table_status)
        send_slack_notification(slack_message)
        update_parameter(parameter_status)
    
def get_message_colour(table_status):
    red = '#ad0614'
    green = '#7CD197'

    if table_status == 'engaged':
        slack_message_colour = red
    else:
        slack_message_colour = green

    return(slack_message_colour)

def create_slack_message(slack_message_colour, table_status):
    slack_message = {
        "icon_url": "https://raw.githubusercontent.com/aws-samples/amazon-guardduty-to-slack/master/images/gd_logo.png",
        "username": "Push4Pool",
        "channel": channel,
        "attachments": [{
            "color": slack_message_colour,
            "title": table_status.upper(),
            "fields": [
                {"value": 'table is now ' + table_status}
            ]
        }
        ]
        
    }

    return(slack_message)

def send_slack_notification(slack_message):
    response = requests.post(
        url, data=json.dumps(slack_message),
        headers={"Content-Type": "application/json"},
    )
    response.raise_for_status()

def update_parameter(parameter):
    if parameter == 'free':
        parameter = ssm.put_parameter(Name='TableStatus', Value='engaged', Type='String', Overwrite=True)
    else:
        parameter = ssm.put_parameter(Name='TableStatus', Value='free', Type='String', Overwrite=True)